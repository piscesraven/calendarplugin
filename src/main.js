import Vue from 'vue'
import App from './App.vue'

// 設定與路由類
import router from './router/'

// console.clear()
new Vue({
  router,
  render: h => h(App)
}).$mount('#app')