import Vue from 'vue'
import Router from 'vue-router'
import { Loading } from 'element-ui'

Vue.use(Router)

let RouteList = [
  {
    path: '',
    component: resolve => require(['@/views/main'], resolve)
  }
]

var router = new Router({
  mode: process.env.NODE_ENV=='development' ? 'history' : 'hash',
  routes: RouteList
})

export default router